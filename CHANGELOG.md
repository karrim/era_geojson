# Changelog

## 0.1.1+5

- Added `==` and `hashCode` to `Feature`

## 0.1.1+4

- Added `toString` to `GeometryCollection`, `MultiLineString`, `MultiPolygon` and `Polygon`

## 0.1.1+3

- Updated `README.md`

## 0.1.1+2

- Fixed `pub` requirements

## 0.1.1+1

- Added `toString` method to `Feature` and `FeatureCollection`
- Added example to `README.md` and in `example` directory.

## 0.1.1

- Created `Feature` and `FeatureCollection`

## 0.1.0

- Created `GeometryCollection`
- Added `fromJson` constructor to `Geometry`

## 0.0.9

- Created `MultiPolygon` class

## 0.0.8

- Created `MultiLineString` class

## 0.0.7

- Created `Polygon` and `LineStringListGeometry`

## 0.0.6

- Created `MultiPoint` class
- Moved `LineString`'s parsing function to class `PointListGeometry`
- Added documentation to `LineString` class

## 0.0.5

- Created `LineString` class

## 0.0.4+2

- Refactored `Point` class

## 0.0.4+1

- Refactored some tests
- Added documentation to `Geometry` and `Point`

## 0.0.4

- Created `Point` class

## 0.0.3

- Moved `getCoordinates` to `Geometry` class

## 0.0.3

- Created `getCoordinates` method
- Created `Geometry` base class 

## 0.0.2+1

- Added `toString` test for `LatLng`
- Increased coverage to 100%
- Added homepage to `pubspec.yaml`

## 0.0.2

- Created `LatLng`

## 0.0.1

- Initial version, created by Stagehand
