# era_geojson.dart

Geojson dart library

## Usage

A simple usage example:

    import 'package:era_geojson/era_geojson.dart';
    
    void main() {
        var json = ...;//geojson data
        var collection = new FeatureCollection.fromJson(json);
        print(collection);
    }
    
## Status

Supports:

- `FeatureCollection`

    - `Feature`

- `Geometry`

    - `Point`
    - `Multipoint`
    - `LineString`
    - `MultiLineString`
    - `Polygon`
    - `MultiPolygon`
    - `GeometryCollection`

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://bitbucket.org/karrim/era_geojson/issues
