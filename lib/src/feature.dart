// Copyright (C) 2016  Andrea Cantafio

//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

library era_geojson.src.feature;

import 'geometry.dart';

class Feature {
  Geometry geometry;

  Map<String, dynamic> properties;

  Feature(this.geometry, this.properties);

  factory Feature.fromJson(Map<String, dynamic> json) {
    if (!json.containsKey('geometry')) {
      throw new ArgumentError('Missing geometry key.');
    }
    final geometry = json['geometry'];
    if (geometry is! Map<String, dynamic>) {
      throw new ArgumentError(
          'Invalid geometry: $geometry. Map<String, dynamic> expected.');
    }
    if (!json.containsKey('properties')) {
      throw new ArgumentError('Missing properties key.');
    }
    final properties = json['properties'];
    if (properties is! Map<String, dynamic>) {
      throw new ArgumentError(
          'Invalid properties: $properties. Map<String, dynamic> expected.');
    }
    return new Feature(new Geometry.fromJson(geometry), properties);
  }

  @override
  int get hashCode {
    return geometry.hashCode ^ properties.hashCode;
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    return other is Feature &&
        this.geometry == other.geometry &&
        this.properties == other.properties;
  }

  @override
  String toString() => 'Feature{geometry: $geometry, properties: $properties}';
}

class FeatureCollection {
  final List<Feature> features;

  FeatureCollection(this.features);

  factory FeatureCollection.fromJson(Map<String, dynamic> json) {
    if (!json.containsKey('features')) {
      throw new ArgumentError('Missing features key.');
    }
    final features = json['features'];
    if (features is! List) {
      throw new ArgumentError('Invalid features: $features. List expected.');
    }
    return new FeatureCollection(features.map((feature) {
      if (feature is! Map<String, dynamic>) {
        throw new ArgumentError(
            'Invalid feature: $feature. Map<String, dynamic> expected.');
      }
      return new Feature.fromJson(feature);
    }).toList(growable: false));
  }

  @override
  String toString() => 'FeatureCollection{features: $features}';
}