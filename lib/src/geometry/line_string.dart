// Copyright (C) 2016  Andrea Cantafio

//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

library era_geojson.src.geometry.line_string;

import 'base.dart';
import 'point.dart';

/// For type [LineString], the [coordinates] member must be a [List] of two or
/// more positions.
class LineString extends Geometry<List<Point>> {
  LineString(List<Point> points) : super(points, GeometryType.LINE_STRING);

  LineString.fromArray(List<List<num>> data)
      : super(PointListGeometry.parseList(data), GeometryType.LINE_STRING);

  LineString.fromJson(Map<String, dynamic> json)
      : super(PointListGeometry.parseJson(json), GeometryType.LINE_STRING);

  @override
  int get hashCode => Geometry.EQUALITY.hash(coordinates) ^ type.hashCode;

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    return other is LineString &&
        Geometry.EQUALITY.equals(coordinates, other.coordinates) &&
        type == other.type;
  }

  @override
  String toString() => 'LineString{coordinates: $coordinates}';
}
