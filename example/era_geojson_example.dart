// Copyright (C) 2016  Andrea Cantafio

//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

library era_geojson.example.era_geojson_example;

import 'package:era_geojson/era_geojson.dart';

void main() {
  final json = {
    "type": "FeatureCollection",
    "features": [
      {
        "geometry": {
          "type": "LineString",
          "coordinates": [
            [
              -74.01503727,
              40.70666089
            ],
            [
              -74.01502119,
              40.70656739
            ],
            [
              -74.01502389,
              40.70649827
            ],
            [
              -74.01504275,
              40.7063966
            ],
            [
              -74.01505344,
              40.70631931
            ],
            [
              -74.01506952,
              40.70622581
            ],
            [
              -74.01519025,
              40.70589846
            ],
            [
              -74.01527024,
              40.70562794
            ]
          ]
        },
        "type": "Feature",
        "properties": {
          "kind": "retaining_wall",
        }
      }
    ]
  };

  final collection = new FeatureCollection.fromJson(json);
  print(collection);
}
