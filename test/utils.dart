library era_geojson.test.utils;

import 'package:test/test.dart';

/// Tests [:==:] and hashCode for given objects.
///
/// [:==:] must be:
///
/// * Total: It must return a boolean for all arguments. It should never throw
///     or return `null`.
///
/// * Reflexive: For all objects `o`, `o == o` must be true.
///
/// * Symmetric: For all objects `o1` and `o2`, `o1 == o2` and `o2 == o1` must
///     either both be true, or both be false.
///
/// * Transitive: For all objects `o1`, `o2`, and `o3`, if `o1 == o2` and
///    `o2 == o3` are true, then `o1 == o3` must be true.
///
/// All objects have hash codes. Hash codes are guaranteed to be the same for
/// objects that are equal when compared using the equality operator [:==:].
///
/// [e], [e1] and [e2] are required to test [:==:] properties and must be equal.
///
/// [d] is used to test if different objects are recognized and must not be
/// equal.
void testEqualsAndHashCode(e, e1, e2, d) {
  expect(() {
    expect(e, equals(e1));
    expect(e.hashCode, equals(e1.hashCode));
    expect(e, equals(e2));
    expect(e.hashCode, equals(e2.hashCode));
    expect(e, isNot(equals(d)));
    expect(e.hashCode, isNot(equals(d.hashCode)));

    // Total
    expect(e == e, isNotNull);
    expect(e == e1, isNotNull);
    expect(e == e2, isNotNull);
    expect(e == d, isNotNull);

    expect(e1 == e, isNotNull);
    expect(e1 == e1, isNotNull);
    expect(e1 == e2, isNotNull);
    expect(e1 == d, isNotNull);

    expect(e2 == e, isNotNull);
    expect(e2 == e1, isNotNull);
    expect(e2 == e2, isNotNull);
    expect(e2 == d, isNotNull);

    expect(d == e, isNotNull);
    expect(d == e1, isNotNull);
    expect(d == e2, isNotNull);
    expect(d == d, isNotNull);

    // Reflexive
    expect(e, equals(e));
    expect(e.hashCode, equals(e.hashCode));
    expect(e1, equals(e1));
    expect(e1.hashCode, equals(e1.hashCode));
    expect(e2, equals(e2));
    expect(e2.hashCode, equals(e2.hashCode));

    // Symmetric
    expect(e == e1, equals(e1 == e));
    expect(e == e2, equals(e2 == e));
    expect(e == d, equals(d == e));

    // Transitive
    expect(e == e1, equals(e1 == e2));
    expect(e == e2, equals(e2 == e1));
    expect(e == d, equals(d == e1));
    expect(e == d, equals(d == e2));
  }, isNot(throws)); // Total
}
