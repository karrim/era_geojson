// Copyright (C) 2016  Andrea Cantafio

//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

library era_geojson.test.latlng;

import 'package:era_geojson/era_geojson.dart' show LatLng;
import 'package:test/test.dart';

import '../utils.dart';

void main() {
  group('LatLng', () {
    group('Constructor', () {
      test('Wrong lat', () {
        expect(() {
          new LatLng(-115.0, 80.0);
        }, throwsArgumentError);

        expect(() {
          new LatLng(128.0, 80.0);
        }, throwsArgumentError);

        expect(() {
          new LatLng(90.001, 80.0);
        }, throwsArgumentError);

        expect(() {
          new LatLng(-90.001, 80.0);
        }, throwsArgumentError);
      });

      test('Wrong lng', () {
        expect(() {
          new LatLng(-16.0, 200.0);
        }, throwsArgumentError);

        expect(() {
          new LatLng(-16.0, -190.0);
        }, throwsArgumentError);

        expect(() {
          new LatLng(-16.0, 180.0001);
        }, throwsArgumentError);

        expect(() {
          new LatLng(-16.0, -180.0001);
        }, throwsArgumentError);
      });

      test('Correct lat and lng', () {
        expect(() {
          final latLng = new LatLng(-15.0, 13.7);
          expect(latLng.lat, equals(-15.0));
          expect(latLng.lng, equals(13.7));
        }, isNot(throws));

        expect(() {
          final latLng = new LatLng(89.0, 178.7);
          expect(latLng.lat, equals(89.0));
          expect(latLng.lng, equals(178.7));
        }, isNot(throws));

        expect(() {
          final latLng = new LatLng(-90.0, 180.0);
          expect(latLng.lat, equals(-90.0));
          expect(latLng.lng, equals(180.0));
        }, isNot(throws));

        expect(() {
          final latLng = new LatLng(90.0, -180.0);
          expect(latLng.lat, equals(90.0));
          expect(latLng.lng, equals(-180.0));
        }, isNot(throws));
      });
    });

    group('Setter', () {
      final latLng = new LatLng(-15.0, 13.7);

      test('Wrong lat', () {
        expect(() {
          latLng.lat = -115.0;
        }, throwsArgumentError);

        expect(() {
          latLng.lat = 128.0;
        }, throwsArgumentError);

        expect(() {
          latLng.lat = 90.001;
        }, throwsArgumentError);

        expect(() {
          latLng.lat = -90.001;
        }, throwsArgumentError);
      });

      test('Wrong lng', () {
        expect(() {
          latLng.lng = 200.0;
        }, throwsArgumentError);

        expect(() {
          latLng.lng = -190.0;
        }, throwsArgumentError);

        expect(() {
          latLng.lng = 180.0001;
        }, throwsArgumentError);

        expect(() {
          latLng.lng = -180.0001;
        }, throwsArgumentError);
      });

      test('Correct lat', () {
        expect(() {
          latLng.lat = -15.0;
          expect(latLng.lat, equals(-15.0));
        }, isNot(throws));

        expect(() {
          latLng.lat = 89.0;
          expect(latLng.lat, equals(89.0));
        }, isNot(throws));

        expect(() {
          latLng.lat = -90.0;
          expect(latLng.lat, equals(-90.0));
        }, isNot(throws));

        expect(() {
          latLng.lat = 90.0;
          expect(latLng.lat, equals(90.0));
        }, isNot(throws));
      });

      test('Correct lng', () {
        expect(() {
          latLng.lng = -13.7;
          expect(latLng.lng, equals(-13.7));
        }, isNot(throws));

        expect(() {
          latLng.lng = -178.7;
          expect(latLng.lng, equals(-178.7));
        }, isNot(throws));

        expect(() {
          latLng.lng = 180.0;
          expect(latLng.lng, equals(180.0));
        }, isNot(throws));

        expect(() {
          latLng.lng = -180.0;
          expect(latLng.lng, equals(-180.0));
        }, isNot(throws));
      });
    });

    test('==() and hashCode', () {
      testEqualsAndHashCode(
          new LatLng(15.37, 29.564),
          new LatLng(15.37, 29.564),
          new LatLng(15.37, 29.564),
          new LatLng(45.8, 27.35));
    });

    test('toString', () {
      final latLng = new LatLng(15.37, 29.564);
      expect(latLng.toString(), equals('LatLng{_lat: 15.37, _lng: 29.564}'));
    });
  });
}
