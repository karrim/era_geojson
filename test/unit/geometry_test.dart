// Copyright (C) 2016  Andrea Cantafio

//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

library era_geojson.test.geometry_type;

import 'package:era_geojson/era_geojson.dart';
import 'package:test/test.dart';

import '../utils.dart';

void main() {
  group('Geometry', () {
    group('getCoordinates', () {
      test('ArgumentError', () {
        expect(() {
          Geometry.getCoordinates(null);
        }, throwsArgumentError);

        expect(() {
          Geometry.getCoordinates({'coordinates': null});
        }, throwsArgumentError);

        expect(() {
          Geometry.getCoordinates({});
        }, throwsArgumentError);
      });

      test('Valid Argument', () {
        var value = [10.3, 15.0];
        expect(Geometry.getCoordinates({'coordinates': value}), equals(value));

        value = {'asd': 'sdf'};
        expect(Geometry.getCoordinates({'coordinates': value}), equals(value));
      });
    });
    group('GeometryType', () {
      test('ArgumentError', () {
        expect(() {
          geometryType(null);
        }, throwsArgumentError);

        expect(() {
          geometryType('');
        }, throwsArgumentError);

        expect(() {
          geometryType('SomeRandomName');
        }, throwsArgumentError);
      });
    });

    group('Point', () {
      group('Constructor', () {
        test('ArgumentError', () {
          expect(() {
            new Point.fromJson({'type': 'Point', 'coordinates': null});
          }, throwsArgumentError);

          expect(() {
            new Point.fromJson({
              'type': 'Point',
              'coordinates': [0.0]
            });
          }, throwsArgumentError);

          expect(() {
            new Point.fromArray([0.0]);
          }, throwsArgumentError);

          expect(() {
            new Point.fromArray([]);
          }, throwsArgumentError);

          expect(() {
            new Point.fromArray(null);
          }, throwsArgumentError);
        });

        test('From Array', () {
          final point = new Point.fromArray([0.1, 1.0]);
          expect(point.coordinates, equals(new LatLng(1.0, 0.1)));
          expect(point.type, equals(GeometryType.POINT));
        });

        test('From Json', () {
          final json = {
            'type': 'Point',
            'coordinates': [100.0, 0.0]
          };

          final point = new Point.fromJson(json);
          expect(point.coordinates, equals(new LatLng(0.0, 100.0)));
          expect(point.type, equals(GeometryType.POINT));
        });
      });

      test('==() and hashCode', () {
        testEqualsAndHashCode(
            new Point(new LatLng(15.0, 33.3)),
            new Point(new LatLng(15.0, 33.3)),
            new Point(new LatLng(15.0, 33.3)),
            new Point(new LatLng(25.8, 21.78)));
      });
    });

    group('LineString', () {
      group('Constructor', () {
        test('ArgumentError', () {
          expect(() {
            new LineString.fromJson(
                {"type": "LineString", "coordinates": null});
          }, throwsArgumentError);

          expect(() {
            new LineString.fromJson({
              "type": "LineString",
              "coordinates": [100.0, 0.0]
            });
          }, throwsArgumentError);
        });

        test('From Array', () {
          final lineString = new LineString.fromArray([
            [100.0, 0.0],
            [101.0, 1.0]
          ]);
          expect(
              lineString.coordinates,
              equals(<Point>[
                new Point(new LatLng(0.0, 100.0)),
                new Point(new LatLng(1.0, 101.0))
              ]));
          expect(lineString.type, equals(GeometryType.LINE_STRING));
        });

        test('From Json', () {
          final json = {
            "type": "LineString",
            "coordinates": [
              [100.0, 0.0],
              [101.0, 1.0]
            ]
          };

          final lineString = new LineString.fromJson(json);
          expect(
              lineString.coordinates,
              equals(<Point>[
                new Point(new LatLng(0.0, 100.0)),
                new Point(new LatLng(1.0, 101.0))
              ]));
          expect(lineString.type, equals(GeometryType.LINE_STRING));
        });
      });
      test('==() and hashCode', () {
        testEqualsAndHashCode(
            new LineString.fromArray([
              [100.0, 0.0],
              [101.0, 1.0]
            ]),
            new LineString.fromArray([
              [100.0, 0.0],
              [101.0, 1.0]
            ]),
            new LineString.fromArray([
              [100.0, 0.0],
              [101.0, 1.0]
            ]),
            new LineString.fromArray([
              [0.0, 0.0],
              [11.0, 12.0]
            ]));
      });
    });

    group('MultiPoint', () {
      group('Constructor', () {
        test('ArgumentError', () {
          expect(() {
            new MultiPoint.fromJson(
                {"type": "MultiPoint", "coordinates": null});
          }, throwsArgumentError);

          expect(() {
            new MultiPoint.fromJson({
              'type': 'Point',
              'coordinates': {
                "type": "MultiPoint",
                "coordinates": [100.0, 0.0]
              }
            });
          }, throwsArgumentError);
        });

        test('From Json', () {
          final json = {
            "type": "MultiPoint",
            "coordinates": [
              [100.0, 0.0],
              [101.0, 1.0]
            ]
          };

          final multiPoint = new MultiPoint.fromJson(json);
          expect(
              multiPoint.coordinates,
              equals(<Point>[
                new Point(new LatLng(0.0, 100.0)),
                new Point(new LatLng(1.0, 101.0))
              ]));
          expect(multiPoint.type, equals(GeometryType.MULTI_POINT));
        });
      });
      test('==() and hashCode', () {
        testEqualsAndHashCode(
            new MultiPoint.fromArray([
              [100.0, 0.0],
              [101.0, 1.0]
            ]),
            new MultiPoint.fromArray([
              [100.0, 0.0],
              [101.0, 1.0]
            ]),
            new MultiPoint.fromArray([
              [100.0, 0.0],
              [101.0, 1.0]
            ]),
            new MultiPoint.fromArray([
              [0.0, 0.0],
              [11.0, 12.0]
            ]));
      });
    });

    group('Polygon', () {
      test('Constructor', () {
        expect(() {
          new Polygon.fromJson({"type": "Polygon", "coordinates": null});
        }, throwsArgumentError);

        expect(() {
          new Polygon.fromJson({
            "type": "Polygon",
            "coordinates": [
              [100.0, 0.0],
              [101.0, 0.0],
              [101.0, 1.0],
              [100.0, 1.0],
              [100.0, 0.0],
              [100.2, 0.2],
              [100.8, 0.2],
              [100.8, 0.8],
              [100.2, 0.8],
              [100.2, 0.2]
            ]
          });
        }, throwsArgumentError);
      });
      test('From Json', () {
        final json = {
          "type": "Polygon",
          "coordinates": [
            [
              [100.0, 0.0],
              [101.0, 0.0],
              [101.0, 1.0],
              [100.0, 1.0],
              [100.0, 0.0]
            ],
            [
              [100.2, 0.2],
              [100.8, 0.2],
              [100.8, 0.8],
              [100.2, 0.8],
              [100.2, 0.2]
            ]
          ]
        };

        final polygon = new Polygon.fromJson(json);
        final expected = [
          new LineString.fromArray([
            [100.0, 0.0],
            [101.0, 0.0],
            [101.0, 1.0],
            [100.0, 1.0],
            [100.0, 0.0]
          ]),
          new LineString.fromArray([
            [100.2, 0.2],
            [100.8, 0.2],
            [100.8, 0.8],
            [100.2, 0.8],
            [100.2, 0.2]
          ])
        ];
        expect(polygon.coordinates, orderedEquals(expected));
        expect(polygon.type, equals(GeometryType.POLYGON));
      });

      test('==() and hashCode', () {
        testEqualsAndHashCode(
            new Polygon([
              new LineString.fromArray([
                [100.0, 0.0],
                [101.0, 0.0],
                [101.0, 1.0],
                [100.0, 1.0],
                [100.0, 0.0]
              ]),
              new LineString.fromArray([
                [100.2, 0.2],
                [100.8, 0.2],
                [100.8, 0.8],
                [100.2, 0.8],
                [100.2, 0.2]
              ])
            ]),
            new Polygon([
              new LineString.fromArray([
                [100.0, 0.0],
                [101.0, 0.0],
                [101.0, 1.0],
                [100.0, 1.0],
                [100.0, 0.0]
              ]),
              new LineString.fromArray([
                [100.2, 0.2],
                [100.8, 0.2],
                [100.8, 0.8],
                [100.2, 0.8],
                [100.2, 0.2]
              ])
            ]),
            new Polygon([
              new LineString.fromArray([
                [100.0, 0.0],
                [101.0, 0.0],
                [101.0, 1.0],
                [100.0, 1.0],
                [100.0, 0.0]
              ]),
              new LineString.fromArray([
                [100.2, 0.2],
                [100.8, 0.2],
                [100.8, 0.8],
                [100.2, 0.8],
                [100.2, 0.2]
              ])
            ]),
            new Polygon([
              new LineString.fromArray([
                [1.0, 0.0],
                [11.0, 0.0],
                [10.0, 0.0],
                [10.0, 1.0],
                [10.0, 0.0]
              ]),
              new LineString.fromArray([
                [120.2, 0.2],
                [101.8, 0.2],
                [10.8, 0.8],
                [11.2, 0.8],
                [10.2, 0.3]
              ])
            ]));
      });
    });

    group('MultiLineString', () {
      test('Constructor', () {
        expect(() {
          new MultiLineString.fromJson(
              {"type": "MultiLineString", "coordinates": null});
        }, throwsArgumentError);

        expect(() {
          new MultiLineString.fromJson({
            "type": "MultiLineString",
            "coordinates": [
              [100.0, 0.0],
              [101.0, 0.0],
              [101.0, 1.0],
              [100.0, 1.0],
              [100.0, 0.0],
              [100.2, 0.2],
              [100.8, 0.2],
              [100.8, 0.8],
              [100.2, 0.8],
              [100.2, 0.2]
            ]
          });
        }, throwsArgumentError);
      });
      test('From Json', () {
        final json = {
          "type": "MultiLineString",
          "coordinates": [
            [
              [100.0, 0.0],
              [101.0, 1.0]
            ],
            [
              [102.0, 2.0],
              [103.0, 3.0]
            ]
          ]
        };

        final multiLine = new MultiLineString.fromJson(json);
        final expected = [
          new LineString.fromArray([
            [100.0, 0.0],
            [101.0, 1.0]
          ]),
          new LineString.fromArray([
            [102.0, 2.0],
            [103.0, 3.0]
          ])
        ];
        expect(multiLine.coordinates, orderedEquals(expected));
        expect(multiLine.type, equals(GeometryType.MULTI_LINE_STRING));
      });

      test('==() and hashCode', () {
        testEqualsAndHashCode(
            new MultiLineString([
              new LineString.fromArray([
                [100.0, 0.0],
                [101.0, 0.0],
                [101.0, 1.0],
                [100.0, 1.0],
                [100.0, 0.0]
              ]),
              new LineString.fromArray([
                [100.2, 0.2],
                [100.8, 0.2],
                [100.8, 0.8],
                [100.2, 0.8],
                [100.2, 0.2]
              ])
            ]),
            new MultiLineString([
              new LineString.fromArray([
                [100.0, 0.0],
                [101.0, 0.0],
                [101.0, 1.0],
                [100.0, 1.0],
                [100.0, 0.0]
              ]),
              new LineString.fromArray([
                [100.2, 0.2],
                [100.8, 0.2],
                [100.8, 0.8],
                [100.2, 0.8],
                [100.2, 0.2]
              ])
            ]),
            new MultiLineString([
              new LineString.fromArray([
                [100.0, 0.0],
                [101.0, 0.0],
                [101.0, 1.0],
                [100.0, 1.0],
                [100.0, 0.0]
              ]),
              new LineString.fromArray([
                [100.2, 0.2],
                [100.8, 0.2],
                [100.8, 0.8],
                [100.2, 0.8],
                [100.2, 0.2]
              ])
            ]),
            new MultiLineString([
              new LineString.fromArray([
                [1.0, 0.0],
                [11.0, 0.0],
                [10.0, 0.0],
                [10.0, 1.0],
                [10.0, 0.0]
              ]),
              new LineString.fromArray([
                [120.2, 0.2],
                [101.8, 0.2],
                [10.8, 0.8],
                [11.2, 0.8],
                [10.2, 0.3]
              ])
            ]));
      });
    });
    group('MultiPolygon', () {
      test('Constructor', () {
        expect(() {
          new MultiPolygon.fromJson(
              {"type": "MultiPolygon", "coordinates": null});
        }, throwsArgumentError);

        expect(() {
          new MultiPolygon.fromJson({
            "type": "MultiPolygon",
            "coordinates": [
              [100.0, 0.0],
              [101.0, 0.0],
              [101.0, 1.0],
              [100.0, 1.0],
              [100.0, 0.0],
              [100.2, 0.2],
              [100.8, 0.2],
              [100.8, 0.8],
              [100.2, 0.8],
              [100.2, 0.2]
            ]
          });
        }, throwsArgumentError);
      });
      test('From Json', () {
        final json = {
          "type": "MultiPolygon",
          "coordinates": [
            [
              [
                [102.0, 2.0],
                [103.0, 2.0],
                [103.0, 3.0],
                [102.0, 3.0],
                [102.0, 2.0]
              ]
            ],
            [
              [
                [100.0, 0.0],
                [101.0, 0.0],
                [101.0, 1.0],
                [100.0, 1.0],
                [100.0, 0.0]
              ],
              [
                [100.2, 0.2],
                [100.8, 0.2],
                [100.8, 0.8],
                [100.2, 0.8],
                [100.2, 0.2]
              ]
            ]
          ]
        };

        final multiPolygon = new MultiPolygon.fromJson(json);
        final expected = [
          new Polygon.fromArray([
            [
              [102.0, 2.0],
              [103.0, 2.0],
              [103.0, 3.0],
              [102.0, 3.0],
              [102.0, 2.0]
            ]
          ]),
          new Polygon.fromArray([
            [
              [100.0, 0.0],
              [101.0, 0.0],
              [101.0, 1.0],
              [100.0, 1.0],
              [100.0, 0.0]
            ],
            [
              [100.2, 0.2],
              [100.8, 0.2],
              [100.8, 0.8],
              [100.2, 0.8],
              [100.2, 0.2]
            ]
          ])
        ];
        expect(multiPolygon.coordinates, orderedEquals(expected));
        expect(multiPolygon.type, equals(GeometryType.MULTI_POLYGON));
      });

      test('==() and hashCode', () {
        testEqualsAndHashCode(
            new MultiPolygon.fromJson({
              "type": "MultiPolygon",
              "coordinates": [
                [
                  [
                    [102.0, 2.0],
                    [103.0, 2.0],
                    [103.0, 3.0],
                    [102.0, 3.0],
                    [102.0, 2.0]
                  ]
                ],
                [
                  [
                    [100.0, 0.0],
                    [101.0, 0.0],
                    [101.0, 1.0],
                    [100.0, 1.0],
                    [100.0, 0.0]
                  ],
                  [
                    [100.2, 0.2],
                    [100.8, 0.2],
                    [100.8, 0.8],
                    [100.2, 0.8],
                    [100.2, 0.2]
                  ]
                ]
              ]
            }),
            new MultiPolygon.fromJson({
              "type": "MultiPolygon",
              "coordinates": [
                [
                  [
                    [102.0, 2.0],
                    [103.0, 2.0],
                    [103.0, 3.0],
                    [102.0, 3.0],
                    [102.0, 2.0]
                  ]
                ],
                [
                  [
                    [100.0, 0.0],
                    [101.0, 0.0],
                    [101.0, 1.0],
                    [100.0, 1.0],
                    [100.0, 0.0]
                  ],
                  [
                    [100.2, 0.2],
                    [100.8, 0.2],
                    [100.8, 0.8],
                    [100.2, 0.8],
                    [100.2, 0.2]
                  ]
                ]
              ]
            }),
            new MultiPolygon.fromJson({
              "type": "MultiPolygon",
              "coordinates": [
                [
                  [
                    [102.0, 2.0],
                    [103.0, 2.0],
                    [103.0, 3.0],
                    [102.0, 3.0],
                    [102.0, 2.0]
                  ]
                ],
                [
                  [
                    [100.0, 0.0],
                    [101.0, 0.0],
                    [101.0, 1.0],
                    [100.0, 1.0],
                    [100.0, 0.0]
                  ],
                  [
                    [100.2, 0.2],
                    [100.8, 0.2],
                    [100.8, 0.8],
                    [100.2, 0.8],
                    [100.2, 0.2]
                  ]
                ]
              ]
            }),
            new MultiPolygon.fromJson({
              "type": "MultiPolygon",
              "coordinates": [
                [
                  [
                    [10.0, 0.0],
                    [11.0, 0.0],
                    [11.0, 1.0],
                    [10.0, 1.0],
                    [102.0, 0.0]
                  ]
                ],
                [
                  [
                    [102.0, 2.0],
                    [103.0, 2.0],
                    [103.0, 3.0],
                    [102.0, 3.0],
                    [102.0, 2.0]
                  ],
                  [
                    [100.2, 0.2],
                    [100.8, 0.2],
                    [100.8, 0.8],
                    [100.2, 0.8],
                    [100.2, 0.2]
                  ]
                ]
              ]
            }));
      });
    });
  });
}
