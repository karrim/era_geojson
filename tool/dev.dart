library tool.dev;

import 'package:dart_dev/dart_dev.dart' show dev, config;

main(List<String> args) async {
  // Perform task configuration here as necessary.

  // Available task configurations:
  // config.analyze
  // config.copyLicense
  // config.coverage
  // config.docs
  // config.examples
  // config.format
  // config.test

  config.analyze.entryPoints = ['lib/', 'test/', 'tool/'];

  config.copyLicense.directories = ['example/', 'lib/'];

  config.coverage.html = true;

  config.format.directories = ['lib/', 'test/', 'tool/'];

  config.test
    ..unitTests = ['test/unit/']
    ..integrationTests = ['test/integration/']
    ..functionalTests = ['test/functional'];

  await dev(args);
}
